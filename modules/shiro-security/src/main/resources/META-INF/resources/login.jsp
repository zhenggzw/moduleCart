<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/style.css"/>
    <jsp:include page="js-include.html"></jsp:include>
</head>
<body>
<shiro:guest>
      <div  style="width: 400px;margin-left: auto;margin-right: auto;">
	<h2>请登陆</h2>	
	<form name="loginform" action="" method="post">
	<div class="ui form segment" >
		<div class="field">
			<label>用户名</label>
			<div class="ui left labeled icon input">
				<input placeholder="Username" type="text"  name="username"> <i
					class="user icon"></i>
				<div class="ui corner label">
					<i class="icon asterisk"></i>
				</div>
			</div>
		</div>
		<div class="field">
			<label>密码</label>
			<div class="ui left labeled icon input">
				<input type="password"  name="password" > <i class="lock icon"></i>
				<div class="ui corner label">
					<i class="icon asterisk"></i>
				</div>
			</div>
		</div>
		<div class="ui error message">
			<div class="header">
			  <shiro:notAuthenticated>We noticed some issues</shiro:notAuthenticated>
			</div>
		</div>
		<input class="ui blue submit button" type="submit" name="submit" value="登录">
	</div>
	</form>
	</div>
</shiro:guest>
 <shiro:authenticated>
    已经登录，<a href="logout">退出</a>
 </shiro:authenticated>
</body>
</html>
