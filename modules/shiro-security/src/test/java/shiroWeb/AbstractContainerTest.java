/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package shiroWeb;
import static org.junit.Assert.assertTrue;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.util.resource.ResourceCollection;
import org.eclipse.jetty.webapp.WebAppContext;
import org.junit.Before;
import org.junit.BeforeClass;
import com.gargoylesoftware.htmlunit.WebClient;
import java.net.BindException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

public abstract class AbstractContainerTest {
    public static final int MAX_PORT = 9200;

    protected static Server server;

    private static int port = 9180;
    
    protected final WebClient webClient = new WebClient();

    @BeforeClass
    public static void startContainer() throws Exception {
        while (server == null && port < MAX_PORT) {
            try {
                server = createAndStartServer(port);
            } catch (BindException e) {
                System.err.printf("Unable to listen on port %d.  Trying next port.", port);
                port++;
            }
        }
        assertTrue(server.isStarted());
    }
    @Before
    public void beforeTest() {
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(true);
    }
    private static Server createAndStartServer(final int port) throws Exception {
    	Server server = new Server(port);    	
    	WebAppContext war = new WebAppContext("src/test/resources/META-INF/webapp","/");
    	war.setDescriptor("src/test/resources/META-INF/web-fragment.xml");    	  	
        server.setHandler(war );	       
        server.start();
        //add jar resources
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Enumeration<URL> resourceEnum = classLoader.getResources("META-INF/resources");
    	Set<Resource> resources=new HashSet<Resource>();
        while(resourceEnum.hasMoreElements()){
    		URL url=resourceEnum.nextElement();
    		//if(!url.toExternalForm().contains("jar!")){
    		//	continue;
    		//}
            resources.add(Resource.newResource(url));
    	}
        if (resources!=null)
        {
            Resource[] collection=new Resource[resources.size()+1];
            int i=0;
            collection[i++]=war.getBaseResource();
            for (Resource resource : resources)
                collection[i++]=resource;
            war.setBaseResource(new ResourceCollection(collection));
        }
        return server;
    }

    protected static String getBaseUri() {
        return "http://localhost:" + port + "/";
    }
}
