package net.flowas.modulecart.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import net.flowas.modulecart.domain.Product;

public interface ProductEndpoint extends CrudEndpoint<Product>{

	// @Produces("application/octet-stream")
	@GET
	@Path("/images/{id:[0-9][0-9]*}.jpg")
	@Produces("image/jpeg")
	public byte[] images(@PathParam("id") Long id) throws Exception;
	
	@GET
	@Path("list.json")
	@Produces("application/json")
	public List<Product> listAll(@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult);

	@GET
	@Path("last.json")
	@Produces("application/json")
	public List<Product> listLast(@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult);

	@GET
	@Path("promos.json")
	@Produces("application/json")
	public List<Product> listPromos(@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult);

}