package net.flowas.modulecart.rest.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import net.flowas.modulecart.domain.Category;
import net.flowas.modulecart.rest.CategoryEndpoint;

/**
 * 
 */
// @Path("/categories")
public class CategoryImpl extends AbstractEndpoint<Category> implements CategoryEndpoint {
	private EntityManager em = Persistence.createEntityManagerFactory("cartPU")
			.createEntityManager();
	/* (non-Javadoc)
	 * @see net.flowas.modulecart.rest.impl.CategoryEndpoint#findById(java.lang.Long)
	 */
	@Override	
	public Category findById(Long id) {
		TypedQuery<Category> findByIdQuery = em
				.createQuery(
						"SELECT DISTINCT c FROM Category c LEFT JOIN FETCH c.parent LEFT JOIN FETCH c.children WHERE c.id = :entityId ORDER BY c.id",
						Category.class);
		findByIdQuery.setParameter("entityId", id);
		Category entity;
		try {
			entity = findByIdQuery.getSingleResult();
		} catch (NoResultException nre) {
			entity = null;
		}
		if (entity == null) {
			//return Response.status(Status.NOT_FOUND).build();
		}
		return entity;
	}

	/* (non-Javadoc)
	 * @see net.flowas.modulecart.rest.impl.CategoryEndpoint#listAll(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Category> listAll(Integer startPosition,
			Integer maxResult) {
		TypedQuery<Category> findAllQuery = em
				.createQuery(
						"SELECT DISTINCT c FROM Category c LEFT JOIN FETCH c.parent LEFT JOIN FETCH c.children ORDER BY c.id",
						Category.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		final List<Category> results = findAllQuery.getResultList();
		return results;
	}

	@Override
	protected Class<Category> getType() {
		return Category.class;
	}
}