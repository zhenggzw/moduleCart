package cartAdmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(CartAdminApplication.class, args);
    }
}
